import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import Home from '../components/Home/Home';
import Navbar from '../components/Navbar/Navbar';
import Login from '../components/Login/Login';
import Signup from '../components/Signup/Signup';
import checkToken from '../resolvers/checkToken';
import Logout from '../components/Logout/Logout';
import Profile from '../components/Profile/Profile';
import EditProfile from '../components/Profile/EditProfile';
import Movies from '../components/Movies/Movies';
import Movie from '../components/Movie/Movie';
import WatchMovie from '../components/WatchMovie/WatchMovie';
import NewMovie from '../components/NewMovie/NewMovie';
import DeleteMovie from '../components/DeleteMovie/DeleteMovie';



class Routes extends Component {


    render(){


        const PrivateRoute = ({component: Component, ...rest}) =>(
            <Route {...rest} render={(props) => (
                checkToken() === true ? <Component {...props}/> : 
                    <Redirect to='/home'/>
                )} />
        )

        return(
            <Router>

                <main>
                    <Navbar/>
                    <Route exact path='/home' component={Home} />
                    <Route exact path='/logout' component={Logout}/>
                    <Route exact path='/login' component={Login} />
                    <Route exact path='/signup' component={Signup} />
                    <PrivateRoute exact path='/profile/:id' component={Profile}  />
                    <PrivateRoute exact path='/profile/edit/:id' component={EditProfile}  />
                  
                    <PrivateRoute exact path='/movies' component={Movies}  />
                    <PrivateRoute exact path='/movie/:id' component={Movie}  />
                    <PrivateRoute exact path='/watch/:id' component={WatchMovie}  />
                    <PrivateRoute exact path='/addmovie' component={NewMovie}  />
                    <PrivateRoute exact path='/movie/delete/:id' component={DeleteMovie}  />
                    
                </main>


            </Router>
        )
    }
}


export default Routes;