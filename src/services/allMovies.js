import axios from 'axios';
import getToken from '../resolvers/getToken';
import constantes from '../const';

export default () => {
    return axios({
        url:constantes.url+'graphql',
        method:'post',
        data:{
            query: `
                query{
                    allMovies{
                        _id,
                        name,
                        year,
                        director,
                        rank,
                        plot,
                        image,
                    
                        duration
                        
                    }
                }
            `
        },headers:{'Authorization': 'JWT ' +getToken()}
    })
}