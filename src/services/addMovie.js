import axios from 'axios';
import getToken from '../resolvers/getToken';
import constantes from '../const';

export default(data)=>{
    let newMovie = `{
        name:"${data.name}",
        director:"${data.director}",
        year:${data.year},
        rating:"${data.rating}",
        genre:"${data.genre}",
        plot:"${data.plot}",
        url:"${data.url}",
        image:"${data.image}",
        duration:"${data.duration}",
        language:"${data.language}",

    }`


    return axios({
        url: constantes.url+'graphql',
        method:'post',
        data:{
            query:`
                mutation{
                    addMovies(data:${newMovie}){
                        _id,
                        name
                    }
                }
            `
        },headers:{'Authorization' : 'JWT ' + getToken()}
    })
}