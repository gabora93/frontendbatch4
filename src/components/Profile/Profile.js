import React, { Component } from 'react';
import { Link } from "react-router-dom";
import singleUser from '../../services/singleUser';

class Profile extends Component {

    state = {
        userData: ''
    }

    componentDidMount() {
        singleUser(this.props.match.params.id).then((resp) => {
            console.log('>>>>',resp)
            this.setState({
                userData: resp.data.data.singleUsers
            })
            console.log(this.state)
        }).catch((err) => {
            console.log(err)
        })
    }

    loadUserData() {
        console.log(this.state)
        var profileData;
        let { name, lastName, email } = this.state.userData;
        if (this.state.userData === '') {
         
                profileData =<div>Loading your Info...</div>
        
        } else {
            profileData = 
                <div>
                    <h1>This is your Profile</h1>
                    <h2>Name: <strong>{name}</strong></h2>
                    <h2>Last Name: <strong>{lastName}</strong></h2>
                    <h2>Email: <strong>{email}</strong></h2>
                    <Link to={`/profile/edit/${this.props.match.params.id}`}>Edit Profile</Link>

                </div>
            
        }
        return profileData
    }

    render() {



        return (
            <div>
            {this.loadUserData()}
            </div>
        )
    }
}

export default Profile;