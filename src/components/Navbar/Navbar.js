import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import NavbarLink from './NavbarLink';
import getToken from '../../resolvers/getToken'
import payload from '../../resolvers/payload'

class Navbar extends Component {


    chargeProfile = () => {
        console.log(this.context)
        const token = getToken()
        if (token !== null) {

            let payLoad = payload(token)
            return (
                <ul className="navbar-nav">

                    <li>
                        <Link className='nav-link' to={`/profile/${payLoad.id}`}>Welcome {payLoad.email}</Link>
                    </li>

                    <NavbarLink to='/home' title='Home' />
                    <NavbarLink to='/logout' title='Logout' />
                    <NavbarLink to='/movies' title='Movies' />
                    <NavbarLink to='/addmovie' title='Add Movie' />

                </ul>
            )
        } else {
            return (
                <ul class="navbar-nav">

                    <NavbarLink to='/signup' title='Signup' />
                    <NavbarLink to='/login' title='Login' />

                </ul>
            )
        }
    }


    render() {
        return (

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">NopalFlix</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">


                    {this.chargeProfile()}



                </div>
            </nav>
        )
    }
}


export default Navbar;