import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyCsWy6WIDoYkfHBzecckS4OXrkpNh9RR0c",
    authDomain: "frontendbatch4.firebaseapp.com",
    databaseURL: "https://frontendbatch4.firebaseio.com",
    projectId: "frontendbatch4",
    storageBucket: "frontendbatch4.appspot.com",
    messagingSenderId: "128074616654"
  };

  export default firebase.initializeApp(config);